package main

import (
	"fmt"
)

func main() {
	//Practice 2 : Program keputusan lulus/tidak berdasarkan perhitungan ujian

	var nilai1, nilai2, nilaiakhir, rata2 float64

	fmt.Println("Masukkan nilai ujian harian 1 : ")
	fmt.Scanln(&nilai1)
	fmt.Println("Masukkan nilai ujian harian 2 : ")
	fmt.Scanln(&nilai2)
	fmt.Println("Masukkan nilai ujian akhir : ")
	fmt.Scanln(&nilaiakhir)
	rata2 = (nilai1 + nilai2 + nilaiakhir) / 3
	fmt.Println("Nilai rata-rata anda adalah : ", rata2)

	if rata2 >= 75 {
		fmt.Println("Selamat anda lulus!")
	} else {
		fmt.Println("Maaf anda tidak lulus")
	}

}
