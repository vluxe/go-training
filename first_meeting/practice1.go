package main

import "fmt"

func main() {
	//Practice 1 : Menghitung luas dan keliling ligkaran

	var r, luas, keliling, pi float64
	pi = 3.14

	fmt.Println("Masukkan panjang jari-jari lingkaran : ")
	fmt.Scanln(&r)
	luas = pi * r * r
	keliling = 2 * pi * r
	fmt.Println("Luas Lingkaran :  ", luas)
	fmt.Println("Keliling Lingkaran :  ", keliling)

}
