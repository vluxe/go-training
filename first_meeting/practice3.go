package main

import (
	"fmt"
)

func main() {
	//Practice 3 : Program perulangan untuk mendeteksi kumpulan data apakah bernilai ganjil atau genap
	angka := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, v := range angka {
		if v%2 == 1 {
			fmt.Printf("Nilai %v : ganjil \n", v)
		} else {
			fmt.Printf("Nilai %v : genap \n", v)
		}
	}
}
